//
//  Person.swift
//  CouchbaseLiteSwiftExample
//
//  Created by Marco Betschart on 19.04.16.
//  Copyright © 2016 MANDELKIND. All rights reserved.
//

import Foundation
import CouchbaseLiteSwift


@objc(Person)
class Person: CLSModel{
	@NSManaged var givenName: String?
	@NSManaged var familyName: String?
	@NSManaged var birthday: Date?
	@NSManaged var age: NSNumber?
}
