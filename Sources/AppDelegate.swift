//
//  AppDelegate.swift
//  CouchbaseLiteSwiftExample
//
//  Created by Marco Betschart on 19.04.16.
//  Copyright © 2016 MANDELKIND. All rights reserved.
//

import UIKit
import CouchbaseLiteSwift


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	var window: UIWindow?

	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool{
		
		let configuration = CLSDatabase.standardConfiguration
		configuration.schemaVersion = 1
		
		configuration.databaseSetup = { (database: CLSDatabase) throws in
			if CLSQuery<CLSModel>(database: database).isEmpty {
				try Person.create(database,from: [
					"givenName": "Bibi" as AnyObject?,
					"familyName": "Blocksberg" as AnyObject?,
					"age": 14 as AnyObject?
					]).save()
				
				try Person.create(database,from: [
					"givenName": "Marco" as AnyObject?,
					"familyName": "Betschart" as AnyObject?,
					"age": 28 as AnyObject?
					]).save()
				
				try Person.create(database,from: [
					"givenName": "Hugh" as AnyObject?,
					"familyName": "Hefner" as AnyObject?,
					"age": 90 as AnyObject?
					]).save()
			}
		}
		
		return true
	}
}
